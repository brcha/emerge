import info
from Package.CMakePackageBase import *

class subinfo( info.infoclass ):
    def setTargets( self ):
        for ver in ['0.5.2', '0.5.3']:
            self.targets[ver] = 'https://github.com/jbeder/yaml-cpp/archive/release-' + ver +'.tar.gz'
            self.targetInstSrc[ver] = 'yaml-cpp-release-' + ver
        self.svnTargets['gitHEAD'] = "[git]https://github.com/jbeder/yaml-cpp.git"

        self.patchToApply[ "0.5.2" ] = ("yaml-cpp_use-pkgconfig.patch", 1)
        self.targetDigests['0.5.2'] = '61edcbb18b106dfc240f151998e233c03bdbf9f3'
        self.targetDigests['0.5.3'] = 'c7cb8fecdcb3470176157d9b8641f24606a1dd69'

        self.shortDescription = "YAML parser and emitter in C++, written around the YAML 1.2 spec"
        self.defaultTarget = 'gitHEAD'

    def setDependencies( self ):
        self.buildDependencies['virtual/base'] = 'default'
        self.buildDependencies['win32libs/boost'] = 'default'


class Package( CMakePackageBase ):
    def __init__( self ):
        CMakePackageBase.__init__( self )
        self.subinfo.options.package.packageName = 'yaml-cpp'
        self.subinfo.options.configure.defines = ' -DBUILD_SHARED_LIBS=1 -DYAML_CPP_BUILD_TOOLS=0'
