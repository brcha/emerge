import info

from Package.AutoToolsPackageBase import *
from Package.PackageBase import *
from Package.VirtualPackageBase import VirtualPackageBase


class subinfo(info.infoclass):
    def setTargets( self ):
        for ver in [ "0.12" ]:
            self.targets[ ver ]= "http://downloads.sourceforge.net/project/opende/ODE/%s/ode-%s.tar.bz2" % (ver, ver)
            self.targetInstSrc[ ver ] = "ode-%s" % ver
        self.targetDigests['0.12'] = '10e7aae6cc6b1afe523ed52e76afd5e06461ea93'
        self.shortDescription = "Open Dynamics Engine SDK"
        self.defaultTarget = "0.12"

    def setDependencies( self ):
        self.dependencies["virtual/base"] = "default"
        if compiler.isMinGW():
            self.buildDependencies["dev-util/msys"] = "default"

class PackageMinGW(AutoToolsPackageBase):
    def __init__( self, **args ):
        AutoToolsPackageBase.__init__(self)
        self.subinfo.options.configure.defines = (
            ' --enable-shared'
            ' --disable-static'
            ' --disable-asserts'
            ' --enable-double-precision'
            ' --disable-demos'
            ' --disable-gyroscopic'
            ' --without-drawstuff'
            )
        self.subinfo.options.configure.cflags = "-I%s/usr/include " % utils.toMSysPath( self.shell.msysdir ) #could cause problems but we need the autotools headers
        self.subinfo.options.configure.ldflags = "-L%s/usr/lib " % utils.toMSysPath( self.shell.msysdir ) #could cause problems but we need the autotools libopt

if compiler.isMinGW():
    class Package(PackageMinGW):
        def __init__( self ):
            PackageMinGW.__init__( self )
else:
    class Package(VirtualPackageBase):
        def __init__( self ):
            VirtualPackageBase.__init__( self )
