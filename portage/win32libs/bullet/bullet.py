import info
from Package.CMakePackageBase import *

class subinfo( info.infoclass ):
    def setTargets( self ):
        for ver in ['2.83.4', '2.83.7']:
            self.targets[ver] = 'https://github.com/bulletphysics/bullet3/archive/' + ver + '.tar.gz'
            self.targetInstSrc[ver] = 'bullet3-' + ver
        self.targetDigests['2.83.4'] = '96b8d18ebecc87e749247702d4022fcc3385ffd6'
        self.targetDigests['2.83.7'] = 'd01f66ca834dd476741c188ef796d8470d4e28dc'
        self.shortDescription = "A 3D Collision Detection and Rigid Body Dynamics Library for games and animation"
        self.defaultTarget = '2.83.4'

    def setDependencies( self ):
        self.buildDependencies['virtual/base'] = 'default'


class Package( CMakePackageBase ):
    def __init__( self ):
        CMakePackageBase.__init__( self )
        self.subinfo.options.package.packageName = 'bullet'
        self.subinfo.options.configure.defines = (
            ' -DINSTALL_LIBS=ON'
            ' -DINSTALL_EXTRA_LIBS=ON'
            ' -DBUILD_SHARED_LIBS=ON'
            ' -DBUILD_CPU_DEMOS=OFF'
            ' -DBUILD_OPENGL3_DEMOS=OFF'
            ' -DBUILD_BULLET2_DEMOS=OFF'
            ' -DUSE_GRAPHICAL_BENCHMARK=OFF'
            ' -DBUILD_BULLET3=OFF'
            ' -DBUILD_EXTRAS=OFF'
            ' -DUSE_DOUBLE_PRECISION=OFF'
            )

    def buildType( self ):
        return 'Release'
