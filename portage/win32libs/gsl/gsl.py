import info

class subinfo(info.infoclass):
    def setTargets( self ):
        self.svnTargets['gitHEAD'] = "[git]https://github.com/ampl/gsl.git"
        self.svnTargets['1.16'] = "[git]https://github.com/ampl/gsl.git||709cc572279e4a56b0e218b834f202c1b3f757af"
        self.svnTargets['2.1'] = "[git]https://github.com/ampl/gsl.git||f62fce0496913cc0df3ad298e1606fc96e857435"
        self.shortDescription = 'GNU Scientific Library'
        self.defaultTarget = 'gitHEAD'
        self.patchToApply[ "1.16" ] = ("gsl_pkgconfig_fix.patch", 1)
        self.patchToApply[ "2.1" ] = ("gsl_pkgconfig_fix_2.1.patch", 1)

    def setDependencies( self ):
        self.buildDependencies['virtual/base'] = 'default'

    def setBuildOptions( self ):
        self.options.configure.defines = " -DGSL_DISABLE_TESTS=1 -DBUILD_SHARED_LIBS=1 "

from Package.CMakePackageBase import *

class Package(CMakePackageBase):
    def __init__( self ):
        CMakePackageBase.__init__( self )
