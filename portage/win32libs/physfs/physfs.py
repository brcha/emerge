import info
from Package.CMakePackageBase import *
from shells import *

class subinfo( info.infoclass ):
    def setTargets( self ):
        for ver in ['2.0.3']:
            self.targets[ver] = 'http://icculus.org/physfs/downloads/physfs-' + ver + '.tar.bz2'
            self.targetInstSrc[ver] = 'physfs-' + ver
        self.targetDigests['2.0.3'] = '327308c777009a41bbabb9159b18c4c0ac069537'

        self.shortDescription = "A library to provide abstract access to various archives"
        self.defaultTarget = '2.0.3'

    def setDependencies( self ):
        self.buildDependencies['virtual/base'] = 'default'
        self.buildDependencies['win32libs/zlib'] = 'default'
        self.buildDependencies['gnuwin32/sed'] = 'default'
        self.buildDependencies['dev-utils/msys'] = 'default'

class Package( CMakePackageBase ):
    def __init__( self ):
        CMakePackageBase.__init__( self )
        self.subinfo.options.package.packageName = 'physfs'
        self.subinfo.options.configure.defines = ' -DPHYSFS_BUILD_TEST=OFF -DPHYSFS_BUILD_WX_TEST=OFF'

    def configure( self, defines=""):
        shell = MSysShell()
        shell.execute(self.sourceDir(), "sed -i -e 's:-Werror::' CMakeLists.txt")
        return CMakePackageBase.configure(self, defines)
