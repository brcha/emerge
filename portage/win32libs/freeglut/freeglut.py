import info
from Package.CMakePackageBase import *

class subinfo( info.infoclass ):
    def setTargets( self ):
        for ver in ['3.0.0']:
            self.targets[ver] = 'http://downloads.sourceforge.net/freeglut/freeglut-' + ver + '.tar.gz'
            self.targetInstSrc[ver] = 'freeglut-' + ver
        self.targetDigests['3.0.0'] = 'fca52242f9344627a30f11487ee42002e6b0dacd'

        self.shortDescription = "Provides functionality for small OpenGL programs"
        self.defaultTarget = '3.0.0'

    def setDependencies( self ):
        self.buildDependencies['virtual/base'] = 'default'


class Package( CMakePackageBase ):
    def __init__( self ):
        CMakePackageBase.__init__( self )
        self.subinfo.options.package.packageName = 'freeglut'


