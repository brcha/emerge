import info

from Package.AutoToolsPackageBase import *
from Package.PackageBase import *
from Package.VirtualPackageBase import VirtualPackageBase


class subinfo(info.infoclass):
    def setTargets( self ):
        for ver in [ "1.5.0" ]:
            self.targets[ ver ]= "http://downloads.sourceforge.net/project/libjpeg-turbo/%s/libjpeg-turbo-%s.tar.gz" % (ver, ver)
            self.targetInstSrc[ ver ] = "libjpeg-turbo-%s" % ver
        self.targetDigests['1.5.0'] = '9adc21b927e48e4c6889e77079f6c1f3eecf98ab'
        self.shortDescription = "MMX, SSE, and SSE2 SIMD accelerated JPEG library"
        self.defaultTarget = "1.5.0"

    def setDependencies( self ):
        self.dependencies["virtual/base"] = "default"
        self.dependencies["dev-util/yasm"] = "default"
        if compiler.isMinGW():
            self.buildDependencies["dev-util/msys"] = "default"

class PackageMinGW(AutoToolsPackageBase):
    def __init__( self, **args ):
        AutoToolsPackageBase.__init__(self)
        self.subinfo.options.configure.defines = (
            ' --disable-static'
            ' --without-java'
            ' --with-mem-srcdst'
            )
        self.subinfo.options.configure.cflags = "-I%s/usr/include " % utils.toMSysPath( self.shell.msysdir ) #could cause problems but we need the autotools headers
        self.subinfo.options.configure.ldflags = "-L%s/usr/lib " % utils.toMSysPath( self.shell.msysdir ) #could cause problems but we need the autotools libopt

if compiler.isMinGW():
    class Package(PackageMinGW):
        def __init__( self ):
            PackageMinGW.__init__( self )
else:
    class Package(VirtualPackageBase):
        def __init__( self ):
            VirtualPackageBase.__init__( self )
