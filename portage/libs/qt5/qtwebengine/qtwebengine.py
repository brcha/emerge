# -*- coding: utf-8 -*-
import EmergeDebug
import info


class subinfo(info.infoclass):
    def setTargets( self ):
        self.versionInfo.setDefaultValues( )

    def setDependencies( self ):
        self.dependencies['win32libs/sqlite'] = 'default'
        self.dependencies['win32libs/icu'] = 'default'
        self.dependencies['libs/qtbase'] = 'default'
        self.dependencies['libs/qtscript'] = 'default'
        self.dependencies['libs/qtdeclarative'] = 'default'
        self.dependencies['libs/qtmultimedia'] = 'default'
        self.dependencies['libs/qtwebchannel'] = 'default'
        self.buildDependencies['dev-util/ruby'] = 'default'
        self.buildDependencies['dev-util/winflexbison'] = 'default'
        self.buildDependencies['gnuwin32/gperf'] = 'default'


from Package.Qt5CorePackageBase import *

class Package( Qt5CorePackageBase ):
    def __init__( self, **args ):
        Qt5CorePackageBase.__init__( self )
        self.supportsNinja = False  # Includes its own ninja, not to be confused with ours
        self.subinfo.options.fetch.checkoutSubmodules = True
        self.subinfo.options.configure.defines = """ "WEBENGINE_CONFIG += use_proprietary_codecs" """
        os.putenv("SQLITE3SRCDIR",EmergeStandardDirs.emergeRoot())
        if compiler.isMinGW():
            self.subinfo.options.configure.defines = """ "QMAKE_CXXFLAGS += -g0 -O3" """

    def compile(self):
        if not ("Paths","Python27") in emergeSettings:
            EmergeDebug.die("Please make sure Paths/Python27 is set in your kdesettings.ini")
        utils.prependPath(emergeSettings.get("Paths","PYTHON27",""))
        return Qt5CorePackageBase.compile(self)
