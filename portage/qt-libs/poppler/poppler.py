# -*- coding: utf-8 -*-
import info
from Package.CMakePackageBase import *

class subinfo(info.infoclass):
    def setTargets( self ):
        self.svnTargets['gitHEAD'] = "git://git.freedesktop.org/git/poppler/poppler"
        for i in [ '0.38.0', '0.42.0', '0.45.0' ]:
            self.targets[ i ] = 'http://poppler.freedesktop.org/poppler-%s.tar.xz' % i
            self.targetInstSrc[ i ] = 'poppler-%s' % i
        self.patchToApply['0.38.0'] = ("poppler-0.38.0-20151130.diff", 1)
        self.targetDigests['0.38.0'] = '62d334116e509d59cd1d8f172f02c0a81e73182f'
        self.targetDigests['0.42.0'] = '4a445d2da6bcf9fd80517b4c881873977b6f49ba'
        self.targetDigests['0.45.0'] = '695b2ffcd5006c1041f3cb9fe7aa874dbd23de86'

        self.shortDescription = "PDF rendering library based on xpdf-3.0"
        self.defaultTarget = "0.38.0"

    def setDependencies( self ):
        self.dependencies['win32libs/freetype'] = 'default'
        self.dependencies['win32libs/openjpeg'] = 'default'
        self.dependencies['win32libs/lcms'] = 'default'
        self.dependencies['win32libs/zlib'] = 'default'
        self.dependencies['win32libs/libjpeg-turbo'] = 'default'
        self.dependencies['win32libs/libpng'] = 'default'
        self.dependencies['win32libs/libcurl'] = 'default'
        self.dependencies['win32libs/tiff'] = 'default'
        self.runtimeDependencies['data/poppler-data'] = 'default'
        self.dependencies['libs/qtbase'] = 'default'

class Package(CMakePackageBase):
    def __init__( self, **args ):
        CMakePackageBase.__init__( self )

        self.subinfo.options.package.packageName = 'poppler'
        self.subinfo.options.configure.defines = (
            ' -DBUILD_GTK_TESTS=OFF'
            ' -DBUILD_QT4_TESTS=OFF'
            ' -DBUILD_QT5_TESTS=OFF'
            ' -DBUILD_CPP_TESTS=OFF'
            ' -DENABLE_SPLASH=ON'
            ' -DENABLE_ZLIB=ON'
            ' -DENABLE_ZLIB_UNCOMPRESS=OFF'
            ' -DENABLE_XPDF_HEADERS=ON'
            ' -DENABLE_LIBCURL=ON'
            ' -DENABLE_CPP=ON'
            ' -DENABLE_UTILS=OFF'
            ' -DSPLASH_CMYK=OFF'
            ' -DUSE_FIXEDPOINT=OFF'
            ' -DUSE_FLOAT=OFF'
            ' -DWITH_Cairo=OFF'
            ' -DWITH_GObjectIntrospection=OFF'
            ' -DWITH_JPEG=OFF'
            ' -DWITH_NSS3=OFF'
            ' -DWITH_PNG=OFF'
            ' -DWITH_Qt4=OFF'
            ' -DWITH_Qt5Core=ON'
            ' -DWITH_TIFF=ON'
        )
